from messages import MessagePerformative as Action


class Automaton:
    """
    Author : Gaël de Léséleuc

    Automaton class
    This class implement a "automaton" (= finite state machine).
    This automaton will be used to respect the negocation protocol

    A state of the automaton is (previous_perform, recieved_perfom) where :
        - previous_perform is the last performative send by self engineer
        - recieved_perform is the new performative send by the other engineer

    Then transition rules defines which performative can be used based on
    previous_perform and recieved_perform

    attr :
        states : list of States object which composed the automaton
        rules : list of transition rules for the automaton
        current_state : pointer to a State object
    """

    class State:
        """
        State class
        This class implement a state of the automaton

        attr:
            previous_perform : MessagePerfomative object - last performative sent by the engineer
            recieved_perform : MessagePerfomative object - last performative recieved by the other engineer
        """
        def __init__(self, previous_action, recieved_action):
            """
            Initialize a state object
            """
            self.previous_action = previous_action
            self.recieved_action = recieved_action

        def __hash__(self):
            """
            Hash a state object so that it can be use as key in dictionary
            """
            return hash((self.previous_action, self.previous_action))

        def __eq__(self, other):
            return (self.previous_action == other.previous_action) and \
                   (self.recieved_action == other.recieved_action)

        def __str__(self):
            return '(' + str(self.previous_action) + ', ' + str(self.recieved_action) + ')'

    def __init__(self, engineer):
        """
        Initialize the automaton
        """
        self.__engineer = engineer  # NOT NEEDED, JUST FOR DEBUG/TEST TIME
        self.__current_state = Automaton.State(Action.WAIT, None)

        self.__states = {
            Automaton.State(Action.WAIT, None),
            Automaton.State(Action.WAIT, Action.WAIT),
            Automaton.State(Action.WAIT, Action.PROPOSE),
            Automaton.State(Action.PROPOSE, Action.ACCEPT),
            Automaton.State(Action.PROPOSE, Action.ASK_WHY),
            Automaton.State(Action.ARGUE, Action.ARGUE),
            Automaton.State(Action.ARGUE, Action.ACCEPT),
            Automaton.State(Action.ARGUE, Action.PROPOSE),
            Automaton.State(Action.ASK_WHY, Action.ARGUE),
            Automaton.State(Action.ACCEPT, Action.COMMIT),
            Automaton.State(Action.COMMIT, Action.COMMIT),
        }

        self.__rules = {
            Automaton.State(Action.WAIT, Action.WAIT): [Action.WAIT],
            Automaton.State(Action.WAIT, Action.BEGIN): [Action.PROPOSE],
            Automaton.State(Action.WAIT, Action.PROPOSE): [Action.ASK_WHY, Action.ACCEPT],
            Automaton.State(Action.PROPOSE, Action.ACCEPT): [Action.COMMIT],
            Automaton.State(Action.PROPOSE, Action.ASK_WHY): [Action.ARGUE],
            Automaton.State(Action.ARGUE, Action.ACCEPT): [Action.COMMIT],
            Automaton.State(Action.ARGUE, Action.ARGUE): [Action.ARGUE, Action.PROPOSE, Action.ACCEPT],
            Automaton.State(Action.ARGUE, Action.PROPOSE): [Action.ACCEPT, Action.ASK_WHY],
            Automaton.State(Action.ASK_WHY, Action.ARGUE): [Action.ARGUE, Action.PROPOSE, Action.ACCEPT],
            Automaton.State(Action.ACCEPT, Action.COMMIT): [Action.COMMIT],
            Automaton.State(Action.COMMIT, Action.COMMIT): [Action.TAKE],
        }

    def get_current_state(self):
        """
        :return: the pointer to the current state
        """
        return self.__current_state

    def is_in_wait_state(self):
        """
        :return: True if the current state is in the wait state
        """
        return self.__current_state.previous_action == Action.WAIT and \
               self.__current_state.recieved_action is None

    def set_initial_state(self, action):
        """
        Initialize the automaton with the performative recieved by the manager
        :param action: must be MessagePerfomative.BEGIN or MessagePerformative.WAIT
        """
        if action == Action.BEGIN:
            self.__current_state = Automaton.State(Action.BEGIN, None)
        elif action == Action.WAIT:
            self.__current_state = Automaton.State(Action.WAIT, None)
        else:
            raise ValueError("Forbidden initial state")

    def update_with_chosen_action(self, new_action):
        """
        Update the automaton once the engineer has chosen a new performative (=action) to send
        :param new_action: MessagePerformative
        """
        if ((new_action == Action.COMMIT) and (self.__current_state.previous_action == Action.ACCEPT)) \
           or new_action == Action.TAKE:
            self.__current_state = Automaton.State(Action.WAIT, None)
        else:
            self.__current_state = Automaton.State(new_action, None)

    def update_with_recieved_action(self, recieved_action):
        """
        Update the automaton once the engineer has recieved a new performative from the other engineer
        :param recieved_action: MessagePerformative
        """
        self.__current_state = Automaton.State(self.__current_state.previous_action, recieved_action)

    def allowed_actions(self):
        """
        :return: list[MessagePerformative] - list of performative/action can the engineer is allowed to take
        in order to respect the negociation protocol
        """
        return self.__rules[self.__current_state]


