from messages import Message, ManagerRequest, MessagePerformative
from preferences import ItemList, Item

class Communication:
    """
    author : Gael de Léséleuc

    Class Communication
    This class implement the communication interface module of an engineer

    attr:
        engineer : pointer toward the Engineer
    """
    def __init__(self, engineer):
        self.__engineer = engineer

    def get_addr(self):
        """
        :return: addr of the agent
        """
        self.__other_engineer_channel = "channel_for_other_engineers"
        self.__addr = self.__engineer.bind(kind="PUSH", alias=self.__other_engineer_channel)
        return self.__addr

    def connect_to_manager_rep_channel(self, addr):
        """
        Connect the Engineer to a "REP" communication channel from the manager
        :param addr: adress of the remote Agent
        """
        self.__manager_channel = "manager_REP_channel"
        self.__engineer.connect(addr, alias=self.__manager_channel)

    def connect_to_engineer_push_channel(self, addr, name):
        """
        Connect the Engineer to a "PUSH" communication channel from an other engineer
        :param addr: adress of the remote Agent
        """
        self.__other_engineer_name = name
        self.__engineer.connect(addr, handler=self.__engineer.handler)

    def get_other_engineer_name(self):
        return self.__other_engineer_name

    def parse_list_items(self, string):
        """
        From a string "item 1, item2, .. itemN"
        :param string: "item 1, item2, .. itemN"
        :return: ["item1", "item2", .., "itemN"]
        """
        return ItemList([Item(name) for name in string.split(',')])

    def ask_manager_list_items(self):
        """
        Send a message to manager REP communication channel to ask the list of items
        """
        reply = self.send_message_to_manager(MessagePerformative.QUERY_REF, ManagerRequest.ITEMS_TO_DISCUSS)
        return self.parse_list_items(reply.get_content())

    def ask_manager_selected_items(self):
        """
        Send a message to manager REP communication channel to ask the list of selected items so far
        """
        reply = self.send_message_to_manager(MessagePerformative.QUERY_REF, ManagerRequest.SELECTED_ITEMS)
        return self.parse_list_items(reply.get_content())

    def inform_manager_to_select_item(self, item):
        """
        Send a message to self PUSH channel (which the manager listnen to)
        to inform the manager we have selected an new item
        :param: Item instance
        """
        self.send_message_to_manager(MessagePerformative.TAKE, str(item))

    def inform_manager_that_ready(self):
        """
        Send a message to manager REP communication channel to inform him that we are ready
        """
        return self.send_message_to_manager(MessagePerformative.INFORM_REF, "READY").get_msg_performative()

    def send_message_to_manager(self, msg_performative, msg_content=""):
        """
        Base function to send a message to manager.
            1/ Create the Message instance
            2/ Log the message
            3/ Send the message
            4/ Return the answer
        :param msg_performative: MessagePerformative instance
        :param msg_content: string
        :return: string - manager's answer
        """
        message = Message(self.__engineer.name, "Manager", msg_performative, msg_content)
        self.__engineer.log_info("to: " + "Manager" + " - " + str(msg_performative) + '(' + str(msg_content) + ')')
        self.__engineer.send(self.__manager_channel, message)
        return self.__engineer.recv(self.__manager_channel)

    def send_message_to_engineer(self, msg_performative, msg_content=""):
        """
        Base function to send a message to the other engineer .
            1/ Create the Message instance
            2/ Log the message
            3/ Send the message
        :param msg_performative: MessagePerformative instance
        :param msg_content: string
        """
        message = Message(self.__engineer.name, self.__other_engineer_name, msg_performative, msg_content)
        self.__engineer.log_info("to: " + self.__other_engineer_name + " - " + str(msg_performative) +
                                 "(" + str(msg_content) + ")")
        self.__engineer.send(self.__other_engineer_channel, message)


