from preferences import Value, CriterionName

class PreferencePolicy:
    """
    Author : Gaël de Léséleuc

    PreferencePolicy class
    This class implement the prefence policy of the engineer.
    It is needed to add some useful functions on top of Prefences instance

    attr :
        engineer : pointer toward the Engineer
        preference : Preference instance
        OWA_items : list of tupple (item, OWA)
            OWA : average weight score that the engineer preference system give to each item
        top_to_accept : agent will only accept an item which is in the top "top_to_accept" of its pref
    """
    def __init__(self, engineer):
        """
        Initialize the preference policy of the engineer
        :param engineer: pointer toward the Engineer
        """
        self.__engineer = engineer
        self.__preferences = None
        self.__OWA_items = []
        self.__top_to_accept = 0.1

    def initialize_preferences(self, preferences):
        """
        Initialize the preferences
        :param preferences: Preferences of the engineer
        """
        self.__engineer.log_info("preferences initialization")
        self.__preferences = preferences
        for item in self.__preferences.get_items():
            self.__OWA_items.append((item, self.__preferences.aggregation_score(item)))

        self.__OWA_items.sort(key=lambda x:x[1], reverse=True)

        for rank, (item, OWA_score) in enumerate(self.__OWA_items):
            self.__engineer.log_info("item rank n°" + str(rank) +": " + str(item) + " - OWA score : " + str(OWA_score))

    def most_preferred(self, item_list):
        """
        :param item_list: ItemList - list of items
        :return: the most preferred item in the item_list based on the engineer preferences
        """
        return self.__preferences.most_preferred(item_list)

    def should_accept(self, item_query):
        """
        :param item_query: ite=
        :return: True if item_query is in the "top_to_accept" first most preferred item of the engineer preferences
        """
        OWA_index_of_item = [idx for idx, (item,val) in enumerate(self.__OWA_items) if item == item_query][0]
        return ((OWA_index_of_item) / len(self.__OWA_items)) <= self.__top_to_accept

    # BELOW : LIST OF GETTER TOWARD SOME PREFERENCES METHOD
    def should_commit(self, item_query):
        return self.should_commit(item_query)

    def evaluate(self, item, criteria):
        return self.__preferences.evaluate(item, criteria)

    def criterion_by_order_of_preference(self):
        return self.__preferences.get_criterion_name_list()

    def is_preferred_criterion(self, criterion1, criterion2):
        return self.__preferences.is_preferred_criterion(criterion1, criterion2)

    def is_preferred_item(self, item_1, item_2):
        return self.__preferences.is_preferred_item(item_1, item_2)

