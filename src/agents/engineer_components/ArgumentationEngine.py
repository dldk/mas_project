from arguments import Argument, CoupleValue, Comparison, ArgumentationGraph
from preferences import CriterionName, Value
from messages import MessagePerformative as Action

class ArgumentationEngine:
    """
    Author : Gaël de Léséleuc

    ArgumentationEngine class
    This class implement the argumentation inference system of the agent.

    attr :
        engineer : pointer toward the Engineer
        argumentation_graph : an ArgumentationGraph instance which is constantly updated during the negociation
        arguments : dict with item as key and list of Argument as value
    """
    def __init__(self, engineer):
        """
        Initialize the argumentation engine
        :param engineer: pointer toward the engineer
        """
        self.__engineer = engineer
        self.__argumentation_graph = ArgumentationGraph()
        self.__arguments = {}

    def get_graph(self):
        """
        :return: the argumentation graph
        """
        return self.__argumentation_graph

    def can_support(self, item):
        """
        :param item: Item
        :return: True if we can produce argument to support the item, False else
        """
        for criterion in self.__engineer.criterion_by_order_of_preference():
            value = self.__engineer.evaluate(item, criterion)
            if value == Value.GOOD or value == Value.VERY_GOOD:
                return True
        return False

    def add_proposal_to_argumentation_graph(self, new_proposed_item, creator_name):
        """
        Used when an new item is proposed during the negociation
            - add the proposed item as an argument in the argumentation graph
            - if there were already other items in discussion, the new item attacked the current item in discussion
            as a result, add the corresponding attack edge in the argumentation graph
        :param new_proposed_item: str - name of the proposed item
        """
        new_proposal_arg = Argument(new_proposed_item)
        last_proposal = self.__argumentation_graph.find_last_proposal()
        self.__argumentation_graph.add_argument(new_proposal_arg, creator_name=creator_name)
        if last_proposal is not None:
            self.__argumentation_graph.add_attack(new_proposal_arg, last_proposal)

    def prepare_to_support_proposal(self, new_proposed_item):
        """
        Generate a list of argument which can be used to supposed a new proposed item
        :param new_proposed_item: Item
        :return: list of all argument that can support this item, sorted by order of importance based on preferences
        """
        self.add_proposal_to_argumentation_graph(str(new_proposed_item), creator_name=self.__engineer.get_name())
        self.__arguments[str(new_proposed_item)] = []
        for criterion in self.__engineer.criterion_by_order_of_preference():
            value = self.__engineer.evaluate(new_proposed_item, criterion)
            if value == Value.GOOD or value == Value.VERY_GOOD:
                new_argument = Argument(str(new_proposed_item), True, [CoupleValue(criterion, value)])
                self.__arguments[str(new_proposed_item)].append(new_argument)

    def prepare_to_attack_proposal(self, new_proposed_item):
        """
        Generate a list of argument which can be used to attack a new proposed item
        :param new_proposed_item: Item - name of the new proposed item
        :return: list of all argument that can attack this item, sorted by order of importance based on preferences
        """
        self.add_proposal_to_argumentation_graph(str(new_proposed_item),
                                                 creator_name=self.__engineer.get_other_engineer_name())
        self.__arguments[str(new_proposed_item)] = []
        for criterion in self.__engineer.criterion_by_order_of_preference():
            value = self.__engineer.evaluate(new_proposed_item, criterion)
            if value == Value.BAD or value == Value.VERY_BAD:
                new_argument = Argument(str(new_proposed_item), False, [CoupleValue(criterion, value)])
                self.__arguments[str(new_proposed_item)].append(new_argument)

    def support_proposal(self, proposed_item):
        """
        Used when the engineer recieve "ASK_WHY" after having proposed a new item
        :param proposed_item: str - name of the item which was proposed
        :return: string - the strongest supportive argument
        """
        support_argument = self.__arguments[proposed_item].pop(0)
        self.__argumentation_graph.add_argument(support_argument, creator_name=self.__engineer.get_name())
        self.__argumentation_graph.add_support(support_argument, Argument(proposed_item))
        return str(support_argument)

    def search_targeted_argument(self, item_name):
        """
        :param item_name: name of the item which is currently discussed
        :return: last argument that were made and concerned this item
        """
        leaf_arguments = self.__argumentation_graph.get_list_of_leafs()
        for argument in leaf_arguments:
            if argument.get_item_concerned() == item_name:
                return argument
        raise KeyError("no argument concerning the item " + item_name)

    def counter_argue(self, item_name):
        """
        :param item_name : string - name of the item on which we want to produce a counter-argurment
        :return: Argument - counter-argurment or None if we can't make a counter-argument
        """
        argument_to_attack = self.search_targeted_argument(item_name)
        criterion_to_attack = argument_to_attack.get_criterion()
        counter_argument = None

        while len(self.__arguments[item_name]) >= 1:
            possible_argument = self.__arguments[item_name].pop(0)
            possible_criterion = possible_argument.get_criterion()

            if self.__engineer.preferred_criterion(possible_criterion, criterion_to_attack) and \
               possible_criterion != criterion_to_attack:

                counter_argument = possible_argument
                counter_argument.add_comparison(Comparison(possible_criterion, criterion_to_attack))
                break

        if counter_argument:
            self.__argumentation_graph.add_argument(counter_argument, creator_name=self.__engineer.get_name())
            self.__argumentation_graph.add_attack(counter_argument, argument_to_attack)

        return counter_argument

    def update_with_recieved_info(self, recieved_action, recieved_content):
        """
        Update the argumentation engine with information recieved from the other engineer.
        If the engineer recieve a proposal : prepare the arguments to attack the proposal if needed
        If the engineer recieve an argument : add the argument in the graph
        :param recieved_action: MessagePerformative
        :param recieved_content: string
        """
        if recieved_action == Action.PROPOSE:
            self.prepare_to_attack_proposal(self.__engineer.current_item_in_discussion())

        if recieved_action == Action.ARGUE:
            recieved_argument = Argument.parse_argument(recieved_content)
            self.__argumentation_graph.add_argument(recieved_argument,
                                                    creator_name=self.__engineer.get_other_engineer_name())
            targeted_argument = self.search_targeted_argument(recieved_argument.get_item_concerned())

            if recieved_argument.is_in_favor() == targeted_argument.is_in_favor():
                self.__argumentation_graph.add_support(recieved_argument, targeted_argument)
            else:
                self.__argumentation_graph.add_attack(recieved_argument, targeted_argument)

    def get_argumentation_graph(self):
        """
        :return: the argumentation graph
        """
        return self.__argumentation_graph

    def get_initiator_name(self, item):
        """
        :param item: Item instance
        :return: name of the engineer which have proposed this item during negotiation
        """
        return self.__argumentation_graph.get_creator_of_arg(Argument(str(item)))
