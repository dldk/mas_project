from messages import MessagePerformative as Action
from preferences import ItemList
from copy import deepcopy
import random

class ActionPolicy:
    """
    Author : Gaël de Léséleuc

    Class ActionPolicy
    This class implement the action policy of the engineer.
    It select a new action based :
    - the possible new actions given by the automaton
    - informations about items that can been accepted given by preference policy
    - argument that can be made given by the argumentation engine

    attr:
        engineer: pointer toward the Engineer
        items_to_discuss : ItemList instance - list of items which can be discussed (provided by the manager)
        items_not_discussed_yet : ItemList instance - list of items which can be discussed but have not yet been
            mentionned during the negociation
        items_in_discussion : ItemList instance - list of items which have been proposed during the negociation but
            not commited yet
        current_item_in_discussion : Item instance - item which is currently discussed in the negocation
        has_been_proposed_by_self : ItemList instance : list of items which have been prosed by the engineer
    """
    def __init__(self, engineer):
        """
        Initialize the action policy
        :param engineer: pointer toward the Engineer instance
        """
        self.__engineer = engineer
        self.__items_to_discuss = ItemList()
        self.__items_not_discussed_yet = ItemList()
        self.__items_in_discussion = ItemList()
        self.__current_item_in_discussion = None
        self.__has_been_proposed_by_self = []

    def initialize_items_to_discuss(self, items_list):
        """
        Store in memory the list of items to be discussed which have been recieved from the manager
        :param items_list: ItemList insance
        """
        self.__items_to_discuss = deepcopy(items_list)
        self.__items_not_discussed_yet = deepcopy(items_list)

    def get_current_item_in_discussion(self):
        """
        :return: current item in discussion
        """
        return self.__current_item_in_discussion

    def get_items_to_discuss(self):
        """
        :return: list of item to discuss given by the manager
        """
        return self.__items_to_discuss

    def get_items_not_discussed_yet(self):
        """
        :return: list of item which can be discussed but have not been mentionned during the negotiation yet
        """
        return self.__items_not_discussed_yet

    def get_items_in_discussion(self):
        """
        :return: list of item which have already been proposed during the negociation
        """
        return self.__items_in_discussion

    def update_memory_with_recieved_info(self, automaton_state, recieved_content=""):
        """
        Update the memory with information recieved from the other engineer
        :param automaton_state: Automaton.State - current state of the engineer automaton
        :param recieved_content: str
        """
        if automaton_state.recieved_action == Action.PROPOSE:
            self.__current_item_in_discussion = self.__items_to_discuss.search(recieved_content)
            self.__items_not_discussed_yet.remove(self.__current_item_in_discussion)
            self.__items_in_discussion.append(self.__current_item_in_discussion)

    def update_memory_with_send_info(self, automaton_state, new_action, new_content=""):
        """
        Update the memory with information sent to the other engineer
        :param automaton_state: Automaton.State - current state of the engineer automaton
        :param recieved_content: str
        """
        if (automaton_state.recieved_action == Action.COMMIT and new_action == Action.COMMIT) or \
           (new_action == Action.TAKE):
            self.__items_in_discussion.remove(self.__current_item_in_discussion)
            self.__current_item_in_discussion = None

        if new_action == Action.PROPOSE:
            self.__current_item_in_discussion = self.__items_to_discuss.search(new_content)
            self.__items_not_discussed_yet.remove(self.__current_item_in_discussion)
            self.__items_in_discussion.append(self.__current_item_in_discussion)
            self.__has_been_proposed_by_self.append(self.__current_item_in_discussion)

    def next_step(self, possible_actions, recieved_content):
        """
        Based on possible actions that can be made, and one content of recieved message
        choose which action to select next, and produce the associated content
        :param possible_actions: MessagePerformative
        :param recieved_content: str
        :return: tupple(MessagePerformation, str)
        """

        if set(possible_actions) == {Action.PROPOSE}:
            return Action.PROPOSE, str(self.__engineer.make_new_proposal())

        if set(possible_actions) == {Action.TAKE}:
            return Action.TAKE, recieved_content

        if set(possible_actions) == {Action.COMMIT}:
            return Action.COMMIT, recieved_content

        if set(possible_actions) == {Action.WAIT}:
            return Action.WAIT, ""

        if set(possible_actions) == {Action.ASK_WHY, Action.ACCEPT}:
            # In this case, the engineer just has recieved a proposal
            # 1/ the engineer look if it should directly accept the item (ie : if it is in top 10)
            # 2/ the engineer look if there is not a previous item that was proposed by the other agent and
            # and which is prefered
            # 3/ if not, the engineer ask the other engineer to argue
            if self.__engineer.should_accept(self.__current_item_in_discussion):
                return Action.ACCEPT, recieved_content

            previous_better_item = self.__engineer.accept_previous_item(self.__current_item_in_discussion)
            if previous_better_item is not None:
                return Action.ACCEPT, str(previous_better_item)

            return Action.ASK_WHY, ""

        if set(possible_actions) == {Action.ARGUE}:
            # Case when we recieved ASK_WHY, the engineer have no choice but to send an argument
            return Action.ARGUE, self.__engineer.support_proposal()

        if set(possible_actions) == {Action.ARGUE, Action.PROPOSE, Action.ACCEPT}:
            # In this case,
            # 1/ the engineer try to produce an argument
            # 2/ if not possible, the engineer try to make a counter proposal
            # 3/ else, the engineer must accept a proposal done by the other agent which is the best for him so far

            new_argument = self.__engineer.counter_argue(str(self.__current_item_in_discussion))
            if new_argument is not None:
                return Action.ARGUE, str(new_argument)

            if self.__engineer.is_the_one_that_have_proposed(self.__current_item_in_discussion):
                new_item_to_proposed = self.__engineer.make_new_proposal(as_good=False)
            else:
                new_item_to_proposed = self.__engineer.make_new_proposal(as_good=True)

            previous_better_item = self.__engineer.accept_previous_item(new_item_to_proposed)

            if previous_better_item is not None:
                return Action.ACCEPT, str(previous_better_item)
            else:
                return Action.PROPOSE, str(new_item_to_proposed)
