from osbrain import Agent
from messages import MessagePerformative, Message, ManagerRequest
from preferences import ItemList, Item

class Manager(Agent):
    """
    author : Gael de Léséleuc

    Manager agent class
    This class implements the manager agent .

    attr :
        list_items : the list of items to discuss
        selected_items : the list of selected items
    """
    def on_init(self):
        """
        Initialize the agent
        """
        self.__list_items = ItemList()
        self.__selected_items = ItemList()
        self.log_info(self.name + " initialized")

        self.__nb_engineers = 0

    def get_name(self):
        return self.name

    def get_addr(self):
        """
        :return: the address of the manager
        """
        # Initialize communication channel
        addr = self.bind(kind="REP", handler=self.answer_to_request)
        return addr

    def initialize_list_items(self, list_items):
        """
        Initialized the list of items to be discuss
        :return: Item list
        """
        self.__list_items = list_items

    def answer_to_request(self, message):
        """
        :param message: Message instance
        :return: appropriate anwser if needed
        """

        performative = message.get_msg_performative()
        sender = message.get_sender()
        content = message.get_content()

        reply = Message(self.name, sender, None, "")

        if performative == MessagePerformative.QUERY_REF:
            if content == ManagerRequest.ITEMS_TO_DISCUSS:
                reply = Message(self.name, sender, MessagePerformative.INFORM_REF, str(self.__list_items))
            if content == ManagerRequest.SELECTED_ITEMS:
                reply = Message(self.name, sender, MessagePerformative.INFORM_REF, str(self.__selected_items))

        if performative == MessagePerformative.TAKE:
            selected_item = self.__list_items.search(content)
            self.__selected_items.append(selected_item)
            reply = Message(self.name, sender, MessagePerformative.INFORM_REF, str("DONE"))

        if performative == MessagePerformative.INFORM_REF:
            if content == "READY":
                self.__nb_engineers += 1
                if self.__nb_engineers == 2:
                    reply = Message(self.name, sender, MessagePerformative.BEGIN, "")
                else:
                    reply = Message(self.name, sender, MessagePerformative.WAIT, "")

        self.log_info("to: " + sender + " - " + str(reply.get_msg_performative()) +
                      "(" + str(reply.get_content()) + ')')
        return reply


