from osbrain import Agent
from preferences import Preferences, CriterionName
from messages import MessagePerformative
from agents.engineer_components import ActionPolicy, Automaton, Communication, \
                                       PreferencePolicy, ArgumentationEngine
from random import random
from copy import deepcopy

class Engineer(Agent):
    """
    author : Gael de Léséleuc

    Engineer agent class
    This class implements the engineer agent .

    attr :
        preferences : the preferences of the agent
    """
    def on_init(self):
        """
        Initialize the agent
        """
        self.log_info(self.name + " initialized")

        # AN ENGINEER IS COMPOSED OF FIVE MODULES
        self.__preference_policy = PreferencePolicy(self)
        self.__automaton = Automaton(self)
        self.__communication = Communication(self)
        self.__action_policy = ActionPolicy(self)
        self.__argumentation_engine = ArgumentationEngine(self)

    def get_name(self):
        """
        :return: str - name of the agent
        """
        return self.name

    def initialize_communication(self, addr_manager, addr_engineer=None, name_engineer=None):
        """
        Connect the engineer to manager REP_channem and other_engineer PUSH_channel
        :param addr_manager: proxy addr of manager
        :param addr_engineer: proxy addr of the other engineer
        :param name_engineer: name of the other engineer
        """
        self.__communication.connect_to_manager_rep_channel(addr_manager)

        if addr_engineer is not None:
            self.__communication.connect_to_engineer_push_channel(addr_engineer, name_engineer)

    def initialize_preference(self, preferences):
        """
        Set the preferences of the agent
        :param preferences: Prefences instances
            if none, create a random preferences system
        """

        if preferences is None:
            preferences = Preferences()
            criterion_list = [CriterionName.PRODUCTION_COST, CriterionName.ENVIRONMENT_IMPACT,
                              CriterionName.CONSUMPTION, CriterionName.DURABILITY,
                              CriterionName.NOISE]
            criterion_list = random.shuffle(criterion_list)
            preferences.set_criterion_name_list(criterion_list)
        else:
            self.__preference_policy.initialize_preferences(preferences)

    def make_new_proposal(self, update_argumentation_graph=True, as_good=False):
        """
        Look for the most preferred item which can be supported among the items which have not
        been discussed yet
        :param update_argumentation_graph: Boolean - use True if the engineer will propose this item in order to
        update the argumentation_graph
        :return: Item (or None if the engineer can not longer propose a item which can be supported)
        """
        item_list = deepcopy(self.__action_policy.get_items_not_discussed_yet())
        new_item_to_propose = None
        while len(item_list) > 0:
            prefered_item = self.__preference_policy.most_preferred(item_list)
            if self.__argumentation_engine.can_support(prefered_item):
                if as_good:
                    if self.__preference_policy.is_preferred_item(prefered_item, self.current_item_in_discussion()):
                        new_item_to_propose = prefered_item
                        break
                else:
                    new_item_to_propose = prefered_item
                    break

            item_list.remove(prefered_item)

        if (new_item_to_propose is not None) and (update_argumentation_graph is True):
            self.__argumentation_engine.prepare_to_support_proposal(new_item_to_propose)

        return new_item_to_propose

    def is_ready_to_propose(self):
        """
        :return: True if the engineer can make a proposal which can be supported
        """
        return self.make_new_proposal(update_argumentation_graph=False) is not None

    def begin_negociation(self):
        """
        Inform the manager that the engineer is ready to make a proposal
        Then, update the engineer state with the anwser of the manager
        """
        performative = self.__communication.inform_manager_that_ready()
        self.update(performative)

    def update(self, recieved_performative=None, recieved_content=None):
        """
        Handle the update of the engineer internal state when new information arrived (ie : when we recieved a message).
        1/ update the automaton state, action_policy and argumentation engine
        2/ use action_policy to choose what to do next
        3/ update again automaton and action_policy state with chosen action
        4/ if necessary, send a message to manager or engineer which contain engineer's new action
        :param recieved_performative: MessagePerformative - performative of the recieved message
        :param recieved_content:  str - content of the recieved message
        """
        # UPDATE AUTOMATON, ACTION_POLICY AND ARGUMENTATION_ENGINE MODULES WITH NEWLY RECIEVED INFORMATION
        self.__automaton.update_with_recieved_action(recieved_performative)
        self.__action_policy.update_memory_with_recieved_info(self.__automaton.get_current_state(), recieved_content)

        if recieved_performative in [MessagePerformative.ARGUE, MessagePerformative.PROPOSE]:
            self.__argumentation_engine.update_with_recieved_info(recieved_performative, recieved_content)

        # ACTION_POLICY MODULE SELECT TO NEXT ACTION TO DO
        new_perform, new_content = self.__action_policy.next_step(possible_actions=self.__automaton.allowed_actions(),
                                                                  recieved_content=recieved_content)

        # UPDATE AUTOMATON, ACTION_POLICY MODULES WITH NEWLY SEND INFORMATION
        # ARGUMENTATION_ENGINE HAS BEEN AUTOMATICLY UPDATES DURING THE SELECTION OF NEW ACTION
        self.__automaton.update_with_chosen_action(new_perform)
        self.__action_policy.update_memory_with_send_info(self.__automaton.get_current_state(),
                                                          new_perform, new_content)

        # IF NECESSARY SEND A MESSAGE TO THE APPROPRIATE AGENT
        if new_perform in [MessagePerformative.TAKE]:
            self.__communication.send_message_to_manager(new_perform, new_content)

        if new_perform in [MessagePerformative.ACCEPT, MessagePerformative.PROPOSE, MessagePerformative.COMMIT,
                           MessagePerformative.ASK_WHY, MessagePerformative.ARGUE]:
            self.__communication.send_message_to_engineer(new_perform, new_content)

    def handler(self, message):
        self.update(message.get_msg_performative(), message.get_content())

    # BELOW : LIST OF FUNCTION WHICH AS REDEFINE SO THAT THE ENGINEER OBJECT CAN BE USE
    # AS AN INTERFACE BETWEEN THE DISTINCT MODULE

    def ask_manager_list_items(self):
        items_to_discuss = self.__communication.ask_manager_list_items()
        self.__action_policy.initialize_items_to_discuss(items_to_discuss)

    def ask_manager_selected_items(self):
        self.__communication.ask_manager_selected_items()

    def inform_manager_to_select_item(self, item):
        self.__communication.inform_manager_to_select_item(item)

    def get_addr(self):
        return self.__communication.get_addr()

    def most_preferred(self, item_list):
        return self.__preference_policy.most_preferred(item_list)

    def allowed_perform_from_current_state(self):
        return self.__automaton.allowed_actions()

    def should_accept(self, item):
        return self.__preference_policy.should_accept(item)

    def accept_previous_item(self, new_item):
        items_in_discussion = self.__action_policy.get_items_in_discussion()
        items_proposed_by_other = [item for item in items_in_discussion
                                        if self.__argumentation_engine.get_initiator_name(item) != self.name]

        for previous_item in items_proposed_by_other:
            if self.__preference_policy.is_preferred_item(previous_item, new_item)\
               and previous_item != new_item:
                return previous_item

        return None

    def evaluate(self, item, criterion):
        return self.__preference_policy.evaluate(item, criterion)

    def current_item_in_discussion(self):
        return self.__action_policy.get_current_item_in_discussion()

    def items_in_discussion(self):
        return self.__action_policy.get_items_in_discussion()

    def criterion_by_order_of_preference(self):
        return self.__preference_policy.criterion_by_order_of_preference()

    def preferred_criterion(self, criterion1, criterion2):
        return self.__preference_policy.is_preferred_criterion(criterion1, criterion2)

    def support_proposal(self):
        return self.__argumentation_engine.support_proposal(str(self.current_item_in_discussion()))

    def counter_argue(self, item_name):
        return self.__argumentation_engine.counter_argue(item_name)

    def get_argumentation_graph(self):
        return self.__argumentation_engine.get_graph()

    def get_other_engineer_name(self):
        return self.__communication.get_other_engineer_name()

    def is_the_one_that_have_proposed(self, item):
        return self.__argumentation_engine.get_initiator_name(item) == self.name

