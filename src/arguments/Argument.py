from .Comparison import Comparison
from .CoupleValue import CoupleValue
from preferences import CriterionName, Value
import re


class Argument:
    """
    author : Gael de Léséleuc

    Argument class
    This class implements the augment.

    attr :
        item_name : the name of the item for which the argument provided support (or nor)
        positive : boolean value (if true positive argument else negative arguement)
        couple_list : list of couple (either Comparaison or CoupleValue instance)
    """
    def __init__(self, item_name, positive=True, couple_list=None):
        """
        Initiliaze an argument
        :param item_name: string - name of the item concerned by the argument
        :param positive: boolean - True if the argument support item, False if the argument attack item
        :param couple_list: list of CoupleValue/Comparison - premises of the argument
        """
        self.__item_name = item_name
        self.__positive = positive
        self.__couple_list = couple_list

    def get_item_concerned(self):
        """
        :return: string - name of the item concerned by the argument
        """
        return self.__item_name

    def get_criterion(self):
        """
        :return: CriterionName - the criterion on which the argument is based
        """
        if self.__couple_list is None:
            return None
        return self.__couple_list[0].get_criterion()

    def is_in_favor(self):
        """
        :return: boolean - True if the argument is supportive, False else
        """
        return self.__positive

    def add_comparison(self, comparison):
        """
        Add a comparison to the premise of the argument
        :param comparison: Comparison instance
        """
        self.__couple_list.append(comparison)

    def __hash__(self):
        if self.__couple_list is None:
            return hash((self.__item_name, self.__positive))
        else:
            return hash((self.__item_name, self.__positive, tuple(self.__couple_list)))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __str__(self):
        """
        Transform an Argument object to its textual representation
        :return: string
        """
        _str = ""
        if self.__couple_list:
            for i in range(len(self.__couple_list)):
                _str += str(self.__couple_list[i])
                if i+1 < len(self.__couple_list):
                    _str += ", "
            _str += " => "

        if not self.__positive:
            _str += "not "

        _str += str(self.__item_name)
        return _str

    def is_proposal(self):
        return self.__couple_list is None

    @classmethod
    def parse_argument(cls, content):
        """
        Transform the textual representation of an argument to an actual Argument object
        :param content: string
        :return: Argument instance
        """
        # An argument must be of one this three forms :
        # 1/ Item
        # 2/ Criterion1 = Value => Item
        # 3/ Criterion1 = Value, Criterion1 > Criterion2 => (not)? Item
        # This function use regex to extract group corresponding to those possible forms
        pattern = "(\w+)?\s?=?\s?(\w+)?\s?,?\s?(\w+)?\s?>?\s?(\w+)?\s?(?:=>)?\s?(not)?\s?(\w+)"
        match = re.search(pattern, content)
        criterion, value, dominant_criterion, dominated_criterion, is_not, item = \
            [match.group(i) for i in range(1, 7)]

        positive = is_not is None

        if criterion is None:
            return cls(item, positive, None)
        if dominated_criterion is None:
            return cls(item, positive, [CoupleValue(CriterionName[criterion], Value[value])])

        return cls(item, positive, [CoupleValue(CriterionName[criterion], Value[value]),
                                                Comparison(CriterionName[dominant_criterion],
                                                           CriterionName[dominated_criterion])])


