import networkx as nx
import matplotlib.pyplot as plt

class ArgumentationGraph:
    """
    author : Gael de Léséleuc

    ArgumentationGraph class
    This class implements the graph of arguments support/attack.

    attr :
        root : the name of the first argument provide
        arguments : dict. key : argument, value :  list of Edge
        edges : list of Edge
    """
    class Edge:
        """
        Edge inner-class
        This class implements the structure of an edge inside the argumentation graph

        attr:
            arg1, arg2 : two nodes (Argument instance)
            is_attacked_by : boolean, True is arg2 is attacked by arg1
            is_supported_by : boolean, True is arg2 is supported by arg1
        """
        def __init__(self, arg1, arg2, attack=True):
            """
            Initialize an edge
            :param arg1: Argument instance
            :param arg2: Argument instance
            :param attack: boolean, True if arg1 attack arg2 / False if arg1 support arg2
            """
            self.arg1 = arg1
            self.arg2 = arg2
            self.is_attacked_by = (attack is True)
            self.is_supported_by = (attack is False)

        def __str__(self):
            if self.is_attacked_by:
                return "ARG(" + str(self.arg1) + ") attack ARG(" + str(self.arg2) + ")"
            if self.is_supported_by:
                return "ARG(" + str(self.arg1) + ") support ARG(" + str(self.arg2) + ")"

    def __init__(self):
        """
        Initialize an argumentation graph
        """
        self.__root = None  # In our toy examples, the graph will be a rooted tree
                            # the root being the first proposal
        self.__arguments = {}  # Nodes of the graph
        self.__edges = []  # Oriented edges of the graph
        self.__arguments_creator = {}

    def __str__(self):
        """
        :return: one line for each edge of the argumentation graph
        """
        str_ = ""
        if self.__root:
            str_ += "GRAPH ROOT :" + str(self.__root) + "\n"
        else:
            str_ += "EMPTY GRAPH"

        for edge in self.__edges:
            str_ += str(edge) + "\n"
        return str_

    def is_empty(self):
        return self.__root is None

    def get_creator_of_arg(self, arg):
        return self.__arguments_creator[arg]

    def find_last_proposal(self):
        """
        By definition the last proposal is a proposal which had not been attack by an other proposal
        :return: Argument node corresponding to the last proposal
        """
        for arg, list_of_edges in self.__arguments.items():
            if arg.is_proposal():
                is_arg_the_last_proposal = True
                for edge in list_of_edges:
                    if edge.arg1.is_proposal():
                        is_arg_the_last_proposal = False
                if is_arg_the_last_proposal:
                    return arg

        return None

    def get_arguments(self):
        return self.__arguments

    def add_argument(self, argument, creator_name=""):
        """
        Add a Argumentation node in the graph
        :param argument: Argument instance
        :param creator_name: Optional, if we want to store which engineer have produced an argument (can be useful
        to plot the graph)
        """
        if not self.__root:
            self.__root = argument

        self.__arguments[argument] = []
        self.__arguments_creator[argument] = creator_name

    def add_attack(self, arg1, arg2):
        """
        Add an attack edge in the graph
        :param arg1: Argument that attack the other Argument
        :param arg2: Argument which is attacked by the other Argument
        """
        is_attack_by_edge = ArgumentationGraph.Edge(arg1, arg2, attack=True)
        if not self.__arguments[arg2]:
            self.__arguments[arg2] = [is_attack_by_edge]
        else:
            self.__arguments[arg2].append(is_attack_by_edge)
        self.__edges.append(is_attack_by_edge)

    def add_support(self, arg1, arg2):
        """
        Add a support edge in the graph
        :param arg1: Argument that support the other Argument
        :param arg2: Argument which is supported by the other Argument
        """
        is_supported_by_edge = ArgumentationGraph.Edge(arg1, arg2, attack=False)

        if not self.__arguments[arg2]:
            self.__arguments[arg2] = [is_supported_by_edge]
        else:
            self.__arguments[arg2].append(is_supported_by_edge)
        self.__edges.append(is_supported_by_edge)

    def get_list_of_leafs(self):
        """
        :return: list of leafs of the graph
        """
        def leaf_of(node):
            edges_list = self.__arguments[node]
            if len(edges_list) == 0:
                return [node]

            list_of_leafs = []
            for edge in edges_list:
                list_of_leafs += leaf_of(edge.arg1)
            return list_of_leafs

        return leaf_of(self.__root)

    def to_networkx(self):
        """
        Use to convert the argumentation graph into a networkx object
        :return: networkx graph
        """
        nx_graph = nx.DiGraph()
        for arg in self.__arguments.keys():
            nx_graph.add_node(str(arg))
            nx_graph.nodes[str(arg)]["name"] = self.__arguments_creator[arg]

        for edge in self.__edges:
            nx_graph.add_edge(str(edge.arg1), str(edge.arg2))
            if edge.is_supported_by:
                nx_graph.edges[str(edge.arg1), str(edge.arg2)]["type"] = "support"
            else:
                nx_graph.edges[str(edge.arg1), str(edge.arg2)]["type"] = "attack"

        return nx_graph

    def plot(self):
        """
        Plot the argumentation graph using networkx library and maptlotlib
        """
        nx_graph = self.to_networkx()
        node_labels_name = nx.get_node_attributes(nx_graph, "name")
        edge_labels = nx.get_edge_attributes(nx_graph, "type")
        pos = nx.spring_layout(nx_graph, iterations=500, scale=10)

        plt.figure()

        # compute offset for drawing attributes
        pos_attribute_name = {}
        for node, coords in pos.items():
            pos_attribute_name[node] = (coords[0], coords[1] + 0.4)

        nx.draw(nx_graph, with_labels=True, pos=pos, font_size=6, arrows=True, )
        nx.draw_networkx_labels(nx_graph, pos_attribute_name, labels=node_labels_name, font_size=6)
        nx.draw_networkx_edge_labels(nx_graph, pos, labels=edge_labels, font_size=6)

        axes = plt.gca()
        axes.set_xlim([-15, 15])
        axes.set_ylim([-15, 15])

        plt.show()

