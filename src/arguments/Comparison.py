class Comparison:
    """
    author : Gael de Léséleuc

    Comparison class
    This class implements a comparison between two criterons.

    attr :
        dominant_criterion : CriterionName instance
        dominated_criterion : CriterionName instance
    such that dominant_criterion > dominated_criterion
    """
    def __init__(self, dominant_criterion, dominated_criterion):
        """
        Initialize the comparison
        :param dominant_criterion: CriterionName
        :param dominated_criterion: CriterionName
        """
        self.__dominant_criterion = dominant_criterion
        self.__dominated_criterion = dominated_criterion

    def __hash__(self):
        return hash((str(self.__dominant_criterion), str(self.__dominated_criterion)))

    def __str__(self):
        return str(self.__dominant_criterion) + ' > ' + str(self.__dominated_criterion)
