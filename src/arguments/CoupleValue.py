class CoupleValue:
    """
    author : Gael de Léséleuc

    CoupleValue class
    This class implements a tupple (Criterion, Value)

    attr:
       criterion : CriterionName instance
       value : Value instance
    """
    def __init__(self, criterion, value):
        """
        Initialize a CouplevALUE
        :param criterion: CriterionName
        :param value: Value
        """
        self.__criterion = criterion
        self.__value = value

    def get_criterion(self):
        return self.__criterion

    def __str__(self):
        return str(self.__criterion) + " = " + str(self.__value)

    def __hash__(self):
        return hash((str(self.__criterion), str(self.__value)))