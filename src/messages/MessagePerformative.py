from enum import IntEnum

class MessagePerformative(IntEnum):
    """
    author: Gael de Léséleuc

    class MessagePerformative:
    enumeration of each possible performative a message can have
    """
    PROPOSE = 0
    ACCEPT = 1
    COMMIT = 2
    ASK_WHY = 3
    ARGUE = 4
    QUERY_REF = 5
    INFORM_REF = 6
    TAKE = 7
    BEGIN = 8
    WAIT = 9


    def __str__(self):
        if self == MessagePerformative.PROPOSE:
            return "PROPOSE"
        if self == MessagePerformative.ACCEPT:
            return "ACCEPT"
        if self == MessagePerformative.ASK_WHY:
            return "ASK_WHY"
        if self == MessagePerformative.ARGUE:
            return "ARGUE"
        if self == MessagePerformative.QUERY_REF:
            return "QUERY_REF"
        if self == MessagePerformative.INFORM_REF:
            return "INFORM_REF"
        if self == MessagePerformative.TAKE:
            return "TAKE"
        if self == MessagePerformative.BEGIN:
            return "BEGIN"
        if self == MessagePerformative.WAIT:
            return "WAIT"
        if self == MessagePerformative.COMMIT:
            return "COMMIT"


