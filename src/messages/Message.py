class Message:
    """
    author : Gael de Léséleuc

    Message class
    This class implements the message object which is exchanged between agent during communication.

    attr :
        from_agent : the sender of the message
        to_agent : the receiver of the message
        message_performative : the performative of the message
        content : the content of the message
    """

    def __init__(self, from_agent, to_agent, message_perfomative, content):
        """
        Create a message
        """
        self.__from_agent = from_agent
        self.__to_agent = to_agent
        self.__message_perfomative = message_perfomative
        self.__content = content

    def get_sender(self):
        """
        :return: sender of the message
        """
        return self.__from_agent

    def get_content(self):
        """
        :return: content of the message
        """
        return self.__content

    def get_msg_performative(self):
        """
        :return: performative of the message
        """
        return self.__message_perfomative

    def __str__(self):
        """
        :return: a string which explicit the all message
        """

        return "(" + self.__from_agent + " to " + self.__to_agent + ": " + \
               str(self.__message_perfomative) + "(" + str(self.__content) + ")" + ")"
