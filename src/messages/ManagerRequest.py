from enum import IntEnum

class ManagerRequest(IntEnum):
    """
    author: Gael de Léséleuc

    class ManagerRequest:
    enumeration of each possible request from an engineer to a manager
    """
    ITEMS_TO_DISCUSS = 0
    SELECTED_ITEMS = 1


    def __str__(self):
        if self == ManagerRequest.ITEMS_TO_DISCUSS:
            return "ITEMS_TO_DISCUSS"
        if self == ManagerRequest.SELECTED_ITEMS:
            return "SELECTED_ITEMS"
