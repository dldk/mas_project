from agents import Engineer, Manager
from preferences import Preferences, ItemList, Item
from osbrain import run_agent
from osbrain import run_nameserver
import time

"""
In this test, we test two agents that give the same value to each items on each criterions
but which have a different priority order on the criteria set 
"""

DATA_PATH = "data/"

if __name__ == "__main__":
    # Initialize server
    ns = run_nameserver()

    # Initialize agents
    engineer_1 = run_agent('Engineer_1', base=Engineer)
    engineer_2 = run_agent('Engineer_2', base=Engineer)
    manager = run_agent('Manager', base=Manager)

    # Load agents preferences
    pref_engineer_1 = Preferences().load_from_csv(DATA_PATH + "pref_engineer1_demo2.csv")
    pref_engineer_2 = Preferences().load_from_csv(DATA_PATH + "pref_engineer2_demo2.csv")

    engineer_1.initialize_preference(pref_engineer_1)
    engineer_2.initialize_preference(pref_engineer_2)

    # Initalize list of items to be discussed and give it to the manager
    list_items = ItemList([Item("DIESEL_ENGINE"), Item("HYBRID_ENGINE"), Item("ESSENCE_ENGINE"),
                           Item("GAS_ENGINE"), Item("ELECTRIC_ENGINE")])

    manager.initialize_list_items(list_items)

    # Get address of remote agents
    addr_manager = manager.get_addr()
    addr_engineer_1 = engineer_1.get_addr()
    addr_engineer_2 = engineer_2.get_addr()

    # Initialize communication
    engineer_1.initialize_communication(addr_manager, addr_engineer_2, engineer_2.get_name())
    engineer_2.initialize_communication(addr_manager, addr_engineer_1, engineer_1.get_name())

    # Ask manager the item_list to discuss
    engineer_1.ask_manager_list_items()
    engineer_2.ask_manager_list_items()

    # Prepare the proposal and begin_negocation
    if engineer_1.is_ready_to_propose():
        engineer_1.begin_negociation()

    if engineer_2.is_ready_to_propose():
        engineer_2.begin_negociation()

    # Close the server
    ns.shutdown()
