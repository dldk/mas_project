class Item:
    """
    author: Gael de Léséleuc

    Item class
    This class implements the objects about which the argumentation will be
    conducted .

    attr :
    name : the name of the item
    description : the description of the item
    """

    def __init__(self, name, description=""):
        """
        Create a new item
        """
        self.__name = name
        self.__description = description

    def get_name(self):
        """
        :return: name of the item
        """
        return self.__name

    def __str__(self):
        return self.__name

    def get_description(self):
        """
        :return: description of the item
        """
        return self.__description

    def get_value(self, agent_pref, criterion):
        """
        :param agent_pref: Preferences object
        :param criterion: CriterionName object
        :return: the value of the item on the criterion "criterion" for agent "agent"
        """
        return agent_pref.evaluate(self, criterion)

    def get_score(self, agent_pref):
        """
        :param agent_pref: Preferences object
        :return: aggregation score of item object for the agent
        """
        return agent_pref.aggregation_score(self)

    def __eq__(self, other):
        return self.get_name() == other.get_name()

    def __hash__(self):
        return hash(self.get_name())
