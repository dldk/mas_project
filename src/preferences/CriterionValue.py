class CriterionValue:
    """
    author: Gael de Léséleuc

    CriterionValue class:
    This class associate an item, a criterion, and a value

    attr :
        item : Item  instance (which represent an engine)
        criterion : CriterionName instance (which represent a single criterion)
        value : Value instance (which value has item on this criterion)
    """

    def __init__(self, item, criterion, value):
        self.__item = item
        self.__criterion = criterion
        self.__value = value

    def get_item(self):
        """
        :return: the item instance
        """
        return self.__item

    def get_criterion(self):
        """
        :return: the criterion instance
        """
        return self.__criterion

    def get_value(self):
        """
        :return: the value instance
        """
        return self.__value

    def unpack(self):
        """
        :return: a tupple item, criterion, value
        """
        return self.__item, self.__criterion, self.__value



