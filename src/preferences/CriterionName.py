from enum import Enum, auto

class CriterionName(Enum):
    """
    author : Gael de Léséleuc

    CriterionName class :
    enumaration of all the different criterions
    """
    PRODUCTION_COST = auto()
    ENVIRONMENT_IMPACT = auto()
    CONSUMPTION = auto()
    DURABILITY = auto()
    NOISE = auto()

    def __str__(self):
        if self == CriterionName.PRODUCTION_COST:
            return "PRODUCTION_COST"
        if self == CriterionName.ENVIRONMENT_IMPACT:
            return "ENVIRONMENT_IMPACT"
        if self == CriterionName.CONSUMPTION:
            return "CONSUMPTION"
        if self == CriterionName.DURABILITY:
            return "DURABILITY"
        if self == CriterionName.NOISE:
            return "NOISE"

    def __iter__(self):
        return [CriterionName.PRODUCTION_COST, CriterionName.ENVIRONMENT_IMPACT, CriterionName.CONSUMPTION,
                CriterionName.DURABILITY, CriterionName.NOISE]

