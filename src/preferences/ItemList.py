class ItemList(list):
    """
    author: Gael de Léséleuc

    ItemList class
    This class is simply used to ovrewrite some function of list
    """
    def __str__(self):
        if len(self) == 0:
            return ""
        if len(self) == 1:
            return str(self[0])
        else:
            return str(self[0]) + "," + ItemList.__str__(self[1:])

    def search(self, query):
        """
        :param query: string, name of the item
        :return: first item which have item_name == query
        """
        for item in self:
            value = item.get_name()
            if query == value:
                return item
        raise KeyError("queried item not is the list")


