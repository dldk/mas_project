from enum import IntEnum

class Value(IntEnum):
    """
    author: Gael de Léséleuc

    class Value:
    enumeration of each possible value a criterion can take
    """
    VERY_BAD = 0
    BAD = 1
    GOOD = 2
    VERY_GOOD = 3

    def __str__(self):
        if self == Value.VERY_BAD:
            return "VERY_BAD"
        if self == Value.BAD:
            return "BAD"
        if self == Value.GOOD:
            return "GOOD"
        if self == Value.VERY_GOOD:
            return "VERY_GOOD"
