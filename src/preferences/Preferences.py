import pandas as pd
from .CriterionName import CriterionName
from .CriterionValue import CriterionValue
from .Item import Item
from .Value import Value

class Preferences:
    """
    author: Gael de Léséleuc

    Preferences class:
    This class represents the preferences of participants
    One participant must be associated with one and only one instance of this class

    attr:
        criterion_name_list: a list of CriterionName
            - all criterion must be present and only once in this list
            - the criterion must be sorted by importance (from most importance to least)

        criterion_value_list: a list of CriterionValue

        weight_list: a list of weight for each CriterionName
            - must be in the same order of criterion_name_list
            - must be descending to reflect the preferences
            (by default : if N is the number of criterion
                [N, N-1, ..., 1]
    """

    def __init__(self):
        self.__criterion_name_list = []
        self.__criterion_value_list = []
        self.__weight_list = []

    def get_items(self):
        return set([criterion_value.get_item() for criterion_value in self.__criterion_value_list])

    def get_item_from_name(self, item_name):
        for item in self.get_items():
            if item.get_name() == item_name:
                return item

    def get_criterion_name_list(self):
        return self.__criterion_name_list

    def load_from_csv(self, path):
        df = pd.read_csv(path, sep=";")
        item_list = df["ITEMS"]
        criterion_name_list = df.columns[1:]
        self.set_criterion_name_list([CriterionName[crit] for crit in criterion_name_list])

        for criterion in criterion_name_list:
            for i, item in enumerate(item_list):
                value = df.loc[i][str(criterion)]
                self.__criterion_value_list.append(CriterionValue(Item(item), CriterionName[criterion], Value[value]))

        return self



    def set_criterion_name_list(self, criterion_name_list):
        """
        Initialize the criterion_name_list
        """
        self.__criterion_name_list = criterion_name_list
        self.__weight_list = list(range(len(criterion_name_list), 0, -1))  # default weight list

    def set_weight_list(self, weight_list):
        """
        To use a custom weight list
        :param weight_list: Int list. Must be strictly decreasing and of len Number_criterion
        """
        self.__weight_list = weight_list

    def add_criterion_value(self, criterion_value):
        """
        Add a criterion_value to an agent preference
        :param criterion_value: CriterionValue instance
        """
        self.__criterion_value_list.append(criterion_value)

    def evaluate(self, item_q, criterion_q):
        """
        :param item_q: Item instance
        :param criterion_q: CriterionName instance
        :return: value of item_q considered for criterion "criterion_q" for this Preference instance
                 if this Preference object has not evaluated this item, raise a KeyError exception
        """
        for criterion_value in self.__criterion_value_list:
            item_k, criterion_k, value = criterion_value.unpack()
            if item_k == item_q and criterion_q == criterion_k:
                return value

        raise KeyError("This preference system has not yet evaluated the queried item on the queried criterion")

    def is_preferred_criterion(self, criterion_1, criterion_2):
        """
        :param criterion_1: CriterionName instance
        :param criterion_2: CriterionName instance
        :return: true if criterion_1 is more than criterion_2 for this Preference instance
                 false else
        """
        return self.__criterion_name_list.index(criterion_1) <= self.__criterion_name_list.index(criterion_2)

    def aggregation_score(self, item_q):
        """
        :param item_q: Item instance
        :return: the aggregation value of item "item" for this Preference instance
        the aggregation value is computed using weight_list
        """
        aggregation_value = 0

        for criterion_value in self.__criterion_value_list:
            if criterion_value.get_item() == item_q:
                value = criterion_value.get_value()
                criterion = criterion_value.get_criterion()
                idx_criterion = self.__criterion_name_list.index(criterion)
                weight = self.__weight_list[idx_criterion]
                aggregation_value += value * weight

        return aggregation_value

    def is_preferred_item(self, item_1, item_2):
        """
        Compare item_1 and item_2 based on their aggregation value for Preference instance

        :param item_1: Item instance
        :param item_2: Item instance
        :return: true if item_1 is preferred to item_2 based on this Preference instance
                 false else
        """
        if item_2 is None:
            return True

        return self.aggregation_score(item_1) >= self.aggregation_score(item_2)

    def most_preferred(self, item_list):
        """
        :param item_list: list of Item instance
        :return: the item which is the most preferred (ie : which has the best aggregation_score
        """
        if len(item_list) == 0:
            return None
        if len(item_list) == 1:
            return item_list[0]
        else:
            item_1 = item_list[0]
            item_2 = self.most_preferred(item_list[1:])
            if self.is_preferred_item(item_1, item_2):
                return item_1
            else:
                return item_2
