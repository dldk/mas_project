from setuptools import setup, find_packages

setup(name='car_engine', version='1.0', packages=find_packages(), install_requires=['osbrain', 'networkx',
                                                                                    'matplotlib'])
