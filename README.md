# MAS PROJECT - CAR ENGINE

### Gaël de Léséleuc de Kérouara

## TO INSTALL THE PROJECT

```
git clone git@gitlab-student.centralesupelec.fr:dldk/mas_project.git
pip install src/
```

## TO RUN THE DEMO

```
python src/run_demo_1.py
python src/run_demo_2.py
```

## PROJECT STRUCTURE

**data folder** contain the preferences of each engineer for the two demo  
**src folder** contain source code of the project : 

- **arguments** module : 
  - *Argument.py* :  implement the argument structure
  - *ArgumentationGraph.py* :  implement the argumentation graph
  - *Comparasion.py* :  implement the comparison between two criterion
  - *CoupleValue.py* : implement the association of a value and a criterion 
- **messages** module:
  - *ManagerRequest.py* : enumeration of performatives used to request the manager
  - *MessagePerformative.py* : enumeration of performatives used  to negotiate 
  - *Message.py* : implement the structure of a message
- **preferences** module:
  - *Item.py* : implement the structure of an item
  - *ItemList.py*: implement a list of items
  - *Value.py*: enumeration of values 
  - *CriterionName.py* : enumeration of criterion 
  - *CriterionValue.py* : implement the association of a value and a criterion for preference system 
  - *Preferences.py*: implement the structure of a preference system
- **agents** module: 
  - *Manager.py*: implement the manager structure
  - *Engineer.py*: implement the engineer structure
  - **engineer_components** submodule:
    - *ActionPolicy.py*: implement the action policy module of an engineer
    - *PreferencePolicy.py*: implement the preference policy module of an engineer
    - *Automaton.py*: implement the automaton module of an engineer
    - *Communication.py*: implement the communication module of an engineer
    - *ArgumentationEngine.py*: implement the argumentation engine module of an engineer
- *run_demo1.py*: implement the first example described in the report
- *run_demo2.py* : implement the second example described in the report 

